from decimal import *

print("Type of 5: ", type(5))
print("Type of 5000 * 50000000: ", type(5000 * 50000000))
print("Type of 'j': ", type('j'))
print("Type of 1j: ", type(1j))
print("Type of (1,): ", type((1,)))
print("Type of [1]: ", type([1]))
print("Type of None: ", type(None))

# print(0.1+0.1+0.1 == 0.3)
print(0.1 + 0.1 + 0.1)
print(Decimal(".1") + Decimal(".1") + Decimal(".1"))


prev_fib = 0
current_fib = 0
for i in range(20):
    if i == 0:
        print(0, end=", ")
    elif i == 1 or i == 2:
        prev_fib = 1
        current_fib = 1
        print(1, end=", ")
    else:
        # next_fib = 
        prev_fib = current_fib
        current_fib = prev_fib + current_fib
        print(current_fib, end=", ")



# size_of_fib = 20
# count = 1
# current_fib = 0
# print(current_fib, end=', ')
# while count <= size_of_fib:
#     current_fib = current_fib + count
#     print(current_fib, end=', ')
#     count += 1


    
