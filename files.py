import fileinput

def copy_file_to(source_file, dest_file):
    try:
        f1 = open(source_file, 'r')
        f2 = open(dest_file, 'w')
        f2.writelines(f1.readlines())
    finally:
        f1.close()
        f2.close()

def cat_files(*inputs, destination):
    with open(destination, 'w') as dest_file_handler:
        with fileinput.input(files=inputs) as source_file_handler:
            lines = [line for line in source_file_handler]
            dest_file_handler.writelines(lines)


def reverse_file_lines(source, destination):
    with open(destination, 'w') as dest_file_handler:
        with fileinput.input(files=source) as source_file_handler:
            # use list comprehension and reverse lines
            lines = [line for line in source_file_handler]
            lines.reverse()
            dest_file_handler.writelines(lines)

def count_file_lines(source) -> int:
    with fileinput.input(files=source) as source_file_handler:
        lines = [line for line in source_file_handler]
        return len(lines)


copy_file_to('text1.txt', 'dest.txt')
cat_files('text1.txt', 'text2.txt', 'Main.java', 'introduction.py', destination='combined_files.txt')
reverse_file_lines('text1.txt', 'reversed_text1.txt')
print(f"text1.txt has {count_file_lines('text1.txt')} lines")

exit()










f = open('buffer.txt', 'a')
try:
    while True:
        user_input = input("Enter input: ")
        if user_input == 'end':
            break
        f.write(user_input + '\n')
finally:
    f.close()

try:
    f = open('buffer.txt', 'r')
    for line in f:
        print(line, end="")
finally:
    f.close()