import random
# x = int(input("Enter a number: "))
# if x % 2 == 0:
#     print("Number is even")
# else:
#     print("Number is odd")

# name = "Jeremiah Nsiah Akuoko"
# age = 20
# print(name)
# print(age)
# print(f"My name is {name} and I'm {age} years old")


# min_guess = 100
# max_guess = 110
# passed = False
# while not passed:
#     guess = int(input(f"Guess a number between {min_guess} and {max_guess} inclusive: "))
#     rand_num = random.randint(min_guess, max_guess)
#     if guess == rand_num:
#         print("Yaaaay, you passed")
#         break
#     else:
#         print("Ooops, you didn't get it. Please try again")

# list1 = [1, 4, 9, 16, 25]
# for x in list1:
#     if x % 2 == 0: print(x)

age = 20
current_age = 1
sum_of_ages = 0
while current_age <= age:
    sum_of_ages += current_age
    current_age += 1

print(f"Sum of years is {sum_of_ages}")
print(f"Numer of months {sum_of_ages * 12}")
print(f"Numer of days {sum_of_ages * 12 * 30}")
print(f"Numer of hours {sum_of_ages * 12 * 30 * 24}")

